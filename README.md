# README #

MPC 3D-LIP based YARP module. 
Walking usage note: The footsteps are generated automatically together with the swinging foot trajectory.

### Installation ###

* cd ~../3dlipmpc
* mkdir build && cd build
* cmake ..
* make
* (Optional) make install


