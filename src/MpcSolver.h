#pragma once

#include <Eigen/Core>
#include "qpOASES.hpp"
#include <vector>
#include <Eigen/Geometry>

enum constraintType
{
  ds,
  ss_left,
  ss_right,
};

class MpcSolver
{
  public:

  MpcSolver(float _predictionTimeStep, float _controlTimeStep, 
                  Eigen::Vector3f com_, Eigen::Vector3f comVel_, Eigen::Vector3f zmp_,  
                  Eigen::Vector3f lFoot_, Eigen::Vector3f lFootVel_, 
                  Eigen::Vector3f rFoot_, Eigen::Vector3f rFootVel_,
                  Eigen::Vector3f footTofoot_, Eigen::Vector3f comVelRef_,
                  float omega_, Eigen::Vector2f footSize_,
                  int N_, int n_, int m_, int q_);
  ~MpcSolver();

  Eigen::VectorXf qpSolution;

  void setReferenceVelocity();
  void computeConstantMatrices();
  void computeVariableMatrices();
  Eigen::VectorXf computeMpc(Eigen::VectorXf state);
  Eigen::VectorXf solveQP();
  void updateState(Eigen::VectorXf optimalInput);
  Eigen::VectorXf getState() const;
  float getLeftFootHeight() const;
  float getRightFootHeight() const;
  void computeSystemMatrices(Eigen::MatrixXf& _A, Eigen::MatrixXf& _B, Eigen::MatrixXf& _C, float timeStep);

  private:

  float predictionTimeStep, controlTimeStep;

  int N; //temporal window
  int n; // state space dim
  int m; // 
  int q; // control space dim

  Eigen::Vector3f com;
  Eigen::Vector3f comVel;
  Eigen::Vector3f zmp;  
  Eigen::Vector3f lFoot;
  Eigen::Vector3f lFootVel;
  Eigen::Vector3f rFoot;  
  Eigen::Vector3f rFootVel;   
  Eigen::Vector3f footTofoot;
  Eigen::Vector3f comVelRef;

  float omega;
  Eigen::Vector2f footSize;
	
  Eigen::VectorXf state;

  Eigen::MatrixXf A, B, C;
  Eigen::MatrixXf A_upd, B_upd, C_upd;

  Eigen::VectorXf maxOutputDoubleSupport, maxOutputSingleSupportLeft, maxOutputSingleSupportRight, maxInput;

  std::vector<constraintType> baseFeetPattern;
  Eigen::VectorXf leftFootHeightPattern, rightFootHeightPattern;

  Eigen::MatrixXf S_bar;
  Eigen::MatrixXf T_bar;
  Eigen::MatrixXf Q_bar;
  Eigen::MatrixXf R_bar;
  Eigen::MatrixXf H;
  Eigen::MatrixXf F_tra;
  Eigen::MatrixXf G;
  Eigen::MatrixXf S;
  Eigen::VectorXf W;

  int controlIter = 0, mpcIter = 0;
  int footstepCounter = 0;

};
