/**
* Copyright: (C) 2017 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/

// Local includes
#include "qpOASES.hpp"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <Eigen/Cholesky>
#include "MPCThread.h"
#include <math.h>  
#include <vector> 


using namespace yarp::os;
using namespace std;

//===============================================
//         Main THREAD
//===============================================

MPCThread::MPCThread(string _name,
			       int _period, float predictionTimeStep_, float controlTimeStep_,
			   Eigen::Vector3f com_, Eigen::Vector3f comVel_, Eigen::Vector3f zmp_,  
                  Eigen::Vector3f lFoot_, Eigen::Vector3f lFootVel_, 
                  Eigen::Vector3f rFoot_, Eigen::Vector3f rFootVel_,
                  Eigen::Vector3f footTofoot_, Eigen::Vector3f comVelRef_,
                  float omega_, Eigen::Vector2f footSize_,
                  int N_, int n_, int m_, int q_)
:   RateThread(_period),
	moduleName(_name),
	_predictionTimeStep(predictionTimeStep_),
	_controlTimeStep(controlTimeStep_),
	N(N_), n(n_), m(m_), q(q_),
	com(com_), comVel(comVel_), zmp(zmp_),
	lFoot(lFoot_), lFootVel(lFootVel_), rFoot(rFoot_), rFootVel(rFootVel_),
	footTofoot(footTofoot_), comVelRef(comVelRef_), 
	omega(omega_), footSize(footSize_),
	printCountdown(0),
	printPeriod(2000)

{

yInfo() << "Launching MPCThread with name : " << _name << " and period " << _period;

}


bool MPCThread::threadInit()
{  
	//opening ports
	com_port.open(string("/"+moduleName+"/com").c_str());
	swfoot_port.open(string("/"+moduleName+"/swfoot").c_str());
	footstep_port.open(string("/"+moduleName+"/footstep").c_str());

	yInfo() << "MPCThread::threadInit finished successfully.";

	return true;
}


void MPCThread::publishCoM()
{
	Bottle& output = com_port.prepare();
	output.clear();

	for (int i=0; i<3; i++){
		output.addDouble(com(i));
	}
  for (int i=0; i<3; i++){
    output.addDouble(comVel(i));
  }
	com_port.write();
}

void MPCThread::publishSwFoot()
{
	Bottle& output = swfoot_port.prepare();
	output.clear();

  for (int i=0; i<3; i++){
    output.addDouble(lFoot(i));
  }
  for (int i=0; i<3; i++){
    output.addDouble(lFootVel(i));
  }
  for (int i=0; i<3; i++){
    output.addDouble(rFoot(i));
  }
  for (int i=0; i<3; i++){
    output.addDouble(rFootVel(i));
  }
	swfoot_port.write();
}

void MPCThread::publishFootSteps()
{
	Bottle& output = footstep_port.prepare();
	output.clear();

	for (int i=0; i<3; i++){
		output.addDouble(footTofoot(i));
	}
	footstep_port.write();
}

//------ RUN ------
void MPCThread::run()
{
	MPCrun();
	double    t = yarp::os::Time::now();
	publishData();
}


void MPCThread::MPCrun()
{
  mpc = new MpcSolver(_predictionTimeStep,  _controlTimeStep, 
                  com, comVel, zmp,  
                  lFoot, lFootVel, 
                  rFoot, rFootVel,
                  footTofoot, comVelRef,
                  omega, footSize,
                  N, n, m, q);

  Eigen::VectorXf state(25);
  state << com(0), comVel(0), zmp(0),
           lFoot(0), lFootVel(0),        
           rFoot(0), rFootVel(0),        
           com(1), comVel(1), zmp(1),  
           lFoot(1), lFootVel(1),     
           rFoot(1), rFootVel(1),     
           footTofoot(1), comVelRef(1),     
           com(2), comVel(2), zmp(2), 
           lFoot(2), lFootVel(2),       
           rFoot(2), rFootVel(2),        
           footTofoot(2), comVelRef(2);

  qpSolution = mpc->computeMpc(state);
  for (int i=0; i<3; i++){
    com(i) = qpSolution(i*9);
    comVel(i) = qpSolution(i*9+1);
    zmp(i) = qpSolution(i*9+2);
    lFoot(i) = qpSolution(i*9+3);
    lFootVel(i) = qpSolution(i*9+4);
    rFoot(i) = qpSolution(i*9+5);
    rFootVel(i) = qpSolution(i*9+6);
    footTofoot(i) = qpSolution(i*9+7);
    comVelRef(i) = qpSolution(i*9+8);
  }
}

void MPCThread::publishData()
{
	publishCoM();
	publishSwFoot();
	publishFootSteps();

	printCountdown = (printCountdown>=printPeriod) ? 0 : printCountdown +(int)getRate();   // countdown for next print (see sendMsg method)
	if( printCountdown == 0 ) {
		double avgTime, stdDev, avgTimeUsed, stdDevUsed;
		getEstPeriod(avgTime, stdDev);
		getEstUsed(avgTimeUsed, stdDevUsed);
	}
}


void MPCThread::closePort()
{
	yInfo() << "Closing com port";
	com_port.interrupt();
	com_port.close();
	yInfo() << "com port closed";
	yInfo() << "Closing swinging foot port";
	swfoot_port.interrupt();
	swfoot_port.close();
	yInfo() << "swinging foot port closed";
	yInfo() << "Closing footstep port";
	footstep_port.interrupt();
	footstep_port.close();
	yInfo() << "footstep port closed";
}

