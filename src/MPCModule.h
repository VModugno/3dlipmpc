/**
* Copyright: (C) 2018 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/

#ifndef __MPC_MODULE_H__
#define __MPC_MODULE_H__

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <string.h>

#include <yarp/os/RFModule.h>
#include <yarp/os/Vocab.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/Bottle.h>
#include <yarp/os/Time.h>
#include <yarp/os/Network.h>
#include <yarp/os/RateThread.h>
#include <yarp/os/Thread.h>
#include <yarp/os/Stamp.h>
#include <yarp/sig/Vector.h>

#include <yarp/os/LogStream.h>

#include "MPCThread.h"


class MPCModule: public yarp::os::RFModule
{
    /* module parameters */
    std::string moduleName;
    int period;

    float _predictionTimeStep, _controlTimeStep;

    int N;
    int n;
    int m;
    int q;

    Eigen::Vector3f com;
    Eigen::Vector3f comVel;
    Eigen::Vector3f zmp;  
    Eigen::Vector3f lFoot;
    Eigen::Vector3f lFootVel;
    Eigen::Vector3f rFoot;  
    Eigen::Vector3f rFootVel;   
    Eigen::Vector3f footTofoot;
    Eigen::Vector3f comVelRef;

    float omega;
    Eigen::Vector2f footSize;


    double  avgTime, stdDev, avgTimeUsed, stdDevUsed;
    
    yarp::os::Port                 rpcPort;        // a port to handle rpc messages
    MPCThread*     mpcThread;     //  mpc solver thread



public:
	MPCModule();

	bool configure(yarp::os::ResourceFinder &rf); // configure all the module parameters and return true if successful    
	bool interruptModule();                       // interrupt, e.g., the ports
	bool close();                                 // close and shut down the module
	bool updateModule();
	
    /**
     * Quit the module.
     * @return true/false on success/failure
     */
	virtual bool quit();
	
};



#endif


