/**
* Copyright: (C) 2018 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/

#ifndef MPC_THREAD
#define MPC_THREAD

// System includes
#include <cstring>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <map>
#include <set>
#include <Eigen/Dense>
#include <Eigen/Core>


// Yarp includes
#include <yarp/os/all.h>
#include <yarp/os/Bottle.h>
#include <yarp/os/PortInfo.h>
#include <yarp/os/Time.h>
#include <yarp/os/LockGuard.h>
#include <yarp/os/Log.h>
#include <yarp/os/LogStream.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/Bottle.h>
#include <yarp/os/ResourceFinder.h>
#include <yarp/os/RateThread.h>
#include <yarp/os/Semaphore.h>
#include <yarp/os/Stamp.h>
#include <yarp/sig/Vector.h>

#include "MpcSolver.h"


class MPCThread: public yarp::os::RateThread
{
	/** prefix for all the ports opened by this module */
	std::string moduleName;
    // thread period in ms
    int period;

    float _predictionTimeStep, _controlTimeStep;

    int N;
    int n;
    int m;
    int q;

    Eigen::Vector3f com;
    Eigen::Vector3f comVel;
    Eigen::Vector3f zmp;  
    Eigen::Vector3f lFoot;
    Eigen::Vector3f lFootVel;
    Eigen::Vector3f rFoot;  
    Eigen::Vector3f rFootVel;   
    Eigen::Vector3f footTofoot;
    Eigen::Vector3f comVelRef;

    float omega;
    Eigen::Vector2f footSize;
	/** helper variable for printing every printPeriod milliseconds */
	int                 printCountdown;
	/** period after which some diagnostic messages are print */
	double              printPeriod;
	yarp::os::Stamp timestamp;

    // ouput ports
    yarp::os::BufferedPort<yarp::os::Bottle> com_port; // 3 com pos; 3 com vel
    yarp::os::BufferedPort<yarp::os::Bottle> swfoot_port; // 3 left foot pos; 3 left foot vel; 3 right foot pos; 3 right foot vel
    yarp::os::BufferedPort<yarp::os::Bottle> footstep_port; // 3 foot to foot distance

    // Rate time controller
    double T;

    MpcSolver* mpc;

	Eigen::VectorXf qpSolution;


	void publishData();
	void MPCrun();
    void publishCoM();
    void publishSwFoot();
    void publishFootSteps();

	  

public:

	MPCThread(std::string _name,
			   int _period, float predictionTimeStep_, float controlTimeStep_,
			   Eigen::Vector3f com_, Eigen::Vector3f comVel_, Eigen::Vector3f zmp_,  
                  Eigen::Vector3f lFoot_, Eigen::Vector3f lFootVel_, 
                  Eigen::Vector3f rFoot_, Eigen::Vector3f rFootVel_,
                  Eigen::Vector3f footTofoot_, Eigen::Vector3f comVelRef_,
                  float omega_, Eigen::Vector2f footSize_,
                  int N_, int n_, int m_, int q_);
	
	bool threadInit();
	void run();
	void closePort();

};


#endif
