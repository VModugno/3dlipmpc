#include "MpcSolver.h"
#include <iostream>
#include "qpOASES.hpp"
#include <vector>
#include "utils.cpp"

static const float infinity = 1e6;

Eigen::MatrixXf blkdiag(std::vector<Eigen::MatrixXf> matrices)
{
  // Compute total matrix size
  int rows = 0, cols = 0;
  for(int i = 0; i < matrices.size(); i++)
  {
    rows += matrices[i].rows();
    cols += matrices[i].cols();
  }

  // Initialize matrix and fill blocks
  Eigen::MatrixXf result = Eigen::MatrixXf::Zero(rows, cols);

  int reachedRow = 0, reachedCol = 0;
  for(int i = 0; i < matrices.size(); i++)
  {
    result.block(reachedRow, reachedCol, matrices[i].rows(), matrices[i].cols()) = matrices[i];
    reachedRow += matrices[i].rows();
    reachedCol += matrices[i].cols();
  }

  return result;
}

MpcSolver::MpcSolver(float _predictionTimeStep, float _controlTimeStep, 
                  Eigen::Vector3f com_, Eigen::Vector3f comVel_, Eigen::Vector3f zmp_,  
                  Eigen::Vector3f lFoot_, Eigen::Vector3f lFootVel_, 
                  Eigen::Vector3f rFoot_, Eigen::Vector3f rFootVel_,
                  Eigen::Vector3f footTofoot_, Eigen::Vector3f comVelRef_,
                  float omega_, Eigen::Vector2f footSize_,
                  int N_, int n_, int m_, int q_)
: N(N_), n(n_), m(m_), q(q_),
  com(com_), comVel(comVel_), zmp(zmp_),
  lFoot(lFoot_), lFootVel(lFootVel_), rFoot(rFoot_), rFootVel(rFootVel_),
  footTofoot(footTofoot_), comVelRef(comVelRef_), 
  omega(omega_), footSize(footSize_)
{
  //omega = sqrtf(9.81f*1000.f/220.f);
  //footSize = Eigen::Vector2f(10.f, 10.f);

  predictionTimeStep = _predictionTimeStep;
  controlTimeStep = _controlTimeStep;

  // // System size
  // N = 20;
  // n = 3*9;
  // m = 3*3;
  // q = 3*6;

  computeSystemMatrices(A, B, C, predictionTimeStep);
  computeSystemMatrices(A_upd, B_upd, C_upd, controlTimeStep);
  // (no tracking but regularization. vref and foot to foot fictitious states to bring the regularization not to zero but to the des value)
  // slide 33 bemporad
  // Initial state
  state.resize(n);
  state << com(0), comVel(0), zmp(0),     // com position, com velocity and zmp position
           lFoot(0), lFootVel(0),        // left foot position and velocity
           rFoot(0), rFootVel(0),        // right foot position and velocity
           footTofoot(0), comVelRef(0),      // foot-to-foot distance (des distance among feet) and vRef (com des vel)
           com(1), comVel(1), zmp(1),     // com position, com velocity and zmp position
           lFoot(1), lFootVel(1),       // left foot position and velocity
           rFoot(1), rFootVel(1),       // right foot position and velocity
           footTofoot(1), comVelRef(1),      // foot-to-foot distance and vRef
           com(2), comVel(2), zmp(2), // com position, com velocity and zmp position
           lFoot(2), lFootVel(2),        // left foot position and velocity
           rFoot(2), rFootVel(2),        // right foot position and velocity
           footTofoot(2), comVelRef(2);     // step height and com target height

  // state << 0, 0, 0,     // com position, com velocity and zmp position
  //          0, 0,        // left foot position and velocity
  //          0, 0,        // right foot position and velocity
  //          0, 100,      // foot-to-foot distance (des distance among feet) and vRef (com des vel)
  //          0, 0, 0,     // com position, com velocity and zmp position
  //          50, 0,       // left foot position and velocity
  //         -50, 0,       // right foot position and velocity
  //         -120, 0,      // foot-to-foot distance and vRef
	 //   220, 0, 220, // com position, com velocity and zmp position
  //          0, 0,        // left foot position and velocity
  //          0, 0,        // right foot position and velocity
  //          35, 220;     // step height and com target height

  maxOutputDoubleSupport.resize(q);
  maxOutputSingleSupportLeft.resize(q);
  maxOutputSingleSupportRight.resize(q);
  maxInput.resize(m);

  maxOutputDoubleSupport <<      infinity, 0, 0, infinity, infinity, 75.f,
				 infinity, 0, 0, infinity, infinity, 20.f,
				 infinity, 0, 0, infinity, infinity, infinity;
  maxOutputSingleSupportLeft <<  infinity, 0, infinity, footSize(0), infinity, 75.f,
				 infinity, 0, infinity, footSize(1), infinity, 20.f,
				 infinity, 0, infinity, infinity, infinity, infinity;
  maxOutputSingleSupportRight << infinity, infinity, 0, infinity, footSize(0), 75.f,
				 infinity, infinity, 0, infinity, footSize(1), 20.f,
				 infinity, infinity, 0, infinity, infinity, infinity;

  maxInput << infinity, infinity, infinity, infinity, infinity, infinity, infinity, infinity, infinity;

  computeConstantMatrices();
}
	
MpcSolver::~MpcSolver() {}

void MpcSolver::computeSystemMatrices(Eigen::MatrixXf& _A, Eigen::MatrixXf& _B, Eigen::MatrixXf& _C, float timeStep)
{
  // LIP model
  Eigen::Matrix3f A_lip;
  Eigen::Vector3f B_lip;

  float ch = coshf(omega*timeStep);
  float sh = sinhf(omega*timeStep);
  A_lip << ch, sh/omega, 1-ch, omega*sh, ch, -omega*sh, 0, 0, 1;
  B_lip << timeStep-sh/omega, 1-ch, timeStep;

  // Foot model
  Eigen::Matrix2f A_foot_cont;
  A_foot_cont << 0, 1, 0, 0;
  Eigen::MatrixXf A_foot = Eigen::MatrixXf::Identity(2,2) + timeStep * A_foot_cont;
  Eigen::MatrixXf B_foot = timeStep * Eigen::Vector2f(0, 1);

  // Dummy states for foot-to-foot distance and reference velocity
  Eigen::MatrixXf A_dummy = Eigen::MatrixXf::Ones(1,1);

  // Full system
  Eigen::MatrixXf B_block = Eigen::MatrixXf::Zero(n/3, m/3);
  B_block.block(0, 0, n/3 - 2, m/3) = blkdiag(std::vector<Eigen::MatrixXf>( {B_lip, B_foot, B_foot} ));

  _A = blkdiag(std::vector<Eigen::MatrixXf>( {A_lip, A_foot, A_foot, A_dummy, A_dummy,
					      A_lip, A_foot, A_foot, A_dummy, A_dummy,
					      A_lip, A_foot, A_foot, A_dummy, A_dummy} ));
  _B = blkdiag(std::vector<Eigen::MatrixXf>( {B_block, B_block, B_block} ));

  Eigen::MatrixXf C_block = Eigen::MatrixXf::Zero(q/3, n/3);

  C_block << 0, 1, 0, 0, 0, 0, 0, 0,-1,  // com velocity to vref 
	     0, 0, 0, 0, 1, 0, 0, 0, 0,  // left foot velocity
	     0, 0, 0, 0, 0, 0, 1, 0, 0,  // left foot velocity
	     0, 0, 1,-1, 0, 0, 0, 0, 0,  // zmp from left foot
	     0, 0, 1, 0, 0,-1, 0, 0, 0,  // zmp from right foot
	     0, 0, 0, 1, 0,-1, 0, 1, 0;  // foot to foot

  Eigen::MatrixXf C_z = Eigen::MatrixXf::Zero(q/3, n/3);

  C_z << 1, 0, 0, 0, 0, 0, 0, 0,-1,  // com position to com target height
	 0, 0, 0, 1, 0, 0, 0, 0, 0,  // left foot position
	 0, 0, 0, 0, 0, 1, 0, 0, 0,  // right foot position
	 0, 0, 0, 1, 0, 0, 0,-1, 0,  // left foot position to step height
	 0, 0, 0, 0, 0, 1, 0,-1, 0,  // right foot position to step height
	 0, 0, 0, 0, 0, 0, 0, 0, 0;  // nothing

  _C = blkdiag(std::vector<Eigen::MatrixXf>( {C_block, C_block, C_z} ));
}

void MpcSolver::computeConstantMatrices()
{
  // Cost function weights
  Eigen::MatrixXf Q = Eigen::MatrixXf::Zero(q, q);
  Eigen::MatrixXf R = Eigen::MatrixXf::Identity(m, m);
  Q(0, 0) = 10000;
  //Q(3, 3) = 100;
  //Q(4, 4) = 100;

  Q(6, 6) = 10000;
  //Q(9, 9) = 100;
  //Q(10, 10) = 100;

  Q(12, 12) = 100;
  Q(15, 15) = 10000;
  Q(16, 16) = 10000;

  S_bar = Eigen::MatrixXf::Zero(q*N, m*N);
  T_bar = Eigen::MatrixXf::Zero(q*N, n);
  Q_bar = Eigen::MatrixXf::Zero(q*N, q*N);
  R_bar = Eigen::MatrixXf::Zero(m*N, m*N);

  // Construct matrices
  for (int i = 0; i < N; i++) //i = 1:N
  {
    for (int j = i; j >= 0; j--)
    {
      S_bar.block(q*i, m*j, q, m) = C * matrixPower(A, i-j) * B;
    }
    
    T_bar.block(q*i, 0, q, n) = C * matrixPower(A, i+1);
    
    Q_bar.block(q*i, q*i, q, q) = Q;
    R_bar.block(m*i, m*i, m, m) = R;
  }

  // Cost function matrices
  H = R_bar + (S_bar.transpose()*Q_bar*S_bar);
  F_tra = T_bar.transpose()*Q_bar*S_bar;

  // Time-invariant constraint matrices
  G.resize(q*N + m*N, m*N);
  S.resize(q*N + m*N, n);
  W.resize(q*N + m*N); //W will be filled later

  G << S_bar, Eigen::MatrixXf::Identity(N*m, N*m);
  S << -T_bar, Eigen::MatrixXf::Zero(N*m, n);

  //baseFeetPattern = std::vector<constraintType>({ss_left, ss_left, ss_left, ss_left, ss_left, ss_left,
		   //ds, ds, ds, ds,
		   //ss_right, ss_right, ss_right, ss_right, ss_right, ss_right,
		   //ds, ds, ds, ds});

  baseFeetPattern = std::vector<constraintType>({
		   ds, ds, ds, ds,
		   ss_left, ss_left, ss_left, ss_left, ss_left, ss_left,
		   ds, ds, ds, ds,
		   ss_right, ss_right, ss_right, ss_right, ss_right, ss_right
		   });

  leftFootHeightPattern.resize(N);
  rightFootHeightPattern.resize(N);

  leftFootHeightPattern << 0.f, 0.f, 0.f, 0.f,
	0.f, 0.f, 0.f, 0.f, 0.f, 0.f,
	0.f, 0.f, 0.f, 0.f,
	0.f, 0.f, 0.f, 0.f, 0.f, 0.f;
	//0.f, 10.f, 20.f, 20.f, 10.f, 0.f;

  rightFootHeightPattern << 0.f, 0.f, 0.f, 0.f,
	0.f, 0.f, 0.f, 0.f, 0.f, 0.f,
	//0.f, 10.f, 20.f, 20.f, 10.f, 0.f,
	0.f, 0.f, 0.f, 0.f,
	0.f, 0.f, 0.f, 0.f, 0.f, 0.f; 
}

void MpcSolver::computeVariableMatrices()
{
  Eigen::VectorXf W_output(q*N);
  Eigen::VectorXf W_input(m*N);

  for (int i = 0; i < N; i++)
  {
    // Pick the right constraint based on the feetPattern
    if(baseFeetPattern[(mpcIter + i) % N] == ds || (footstepCounter == 0 && mpcIter + i < N/2))
      W_output.segment(q*i, q) = maxOutputDoubleSupport;
    else if(baseFeetPattern[(mpcIter + i) % N] == ss_left)
      W_output.segment(q*i, q) = maxOutputSingleSupportLeft;
    else
      W_output.segment(q*i, q) = maxOutputSingleSupportRight;

    W_input.segment(m*i, m) = maxInput;
  }

  W << W_output, W_input;
}

Eigen::VectorXf MpcSolver::solveQP()
{
  Eigen::MatrixXf costFunctionH = H;
  Eigen::VectorXf costFunctionF = state.transpose() * F_tra;
  Eigen::MatrixXf AConstraint = G;
  Eigen::VectorXf bConstraintMin = S * state - W;
  Eigen::VectorXf bConstraintMax = S * state + W;

  qpOASES::QProblem qp;

  int nVariables = (int)costFunctionH.rows();
  int nConstraints = (int)AConstraint.rows();

  qpOASES::real_t H[nVariables*nVariables];
  qpOASES::real_t g[nVariables];

  qpOASES::real_t A[nConstraints*nVariables];
  qpOASES::real_t lb[nConstraints];
  qpOASES::real_t ub[nConstraints];

  for(int i=0;i<nVariables;++i)
  {
    for(int j=0;j<nVariables;++j)
    {
      H[i*nVariables+j] = costFunctionH(i,j);
    }
    g[i] = costFunctionF(i);
  }

  for(int i=0;i<nConstraints;++i)
  {
    for(int j=0;j<nVariables;++j)
    {
      A[i*nVariables+j] = AConstraint(i,j);
    }
    lb[i] = bConstraintMin(i);
    ub[i] = bConstraintMax(i);
  }

  qpOASES::real_t xOpt[nVariables];

  qpOASES::Options options;
  options.setToMPC();
  options.printLevel=qpOASES::PL_NONE;
  qpOASES::int_t nWSR = 300;

  qp = qpOASES::QProblem(nVariables, nConstraints);
  qp.setOptions(options);
  qp.init(H,g,A,0,0,lb,ub,nWSR,NULL,NULL,NULL,NULL,NULL,NULL);

  qp.getPrimalSolution(xOpt);

  Eigen::VectorXf decisionVariables(nVariables);

  for(int i=0;i<nVariables;++i)
  {
    decisionVariables(i) = (float)xOpt[i];
  }

  return decisionVariables;
}

void MpcSolver::updateState(Eigen::VectorXf optimalInput)
{
  state = A_upd * state + B_upd * optimalInput;
}

Eigen::VectorXf MpcSolver::computeMpc(Eigen::VectorXf state)
{
  computeVariableMatrices();
  qpSolution = solveQP();
  updateState(qpSolution.segment(0, m));

  controlIter++;
  mpcIter = (int)floor(controlIter*controlTimeStep/predictionTimeStep);
  if(mpcIter>=N)
  {
    controlIter = 0;
    mpcIter = 0;
    footstepCounter++;
  }
  return qpSolution;
}

Eigen::VectorXf MpcSolver::getState() const
{
  return state;
}

float MpcSolver::getLeftFootHeight() const
{
  return leftFootHeightPattern(mpcIter);
}

float MpcSolver::getRightFootHeight() const
{
  return rightFootHeightPattern(mpcIter);
}

