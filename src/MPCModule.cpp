/**
* Copyright: (C) 2018 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/

#include "MPCModule.h"
#include "MPCThread.h"

using namespace yarp::os;

MPCModule::MPCModule()
{
    mpcThread      = 0;
    period          = 10;
}


bool MPCModule::configure(yarp::os::ResourceFinder &rf) 
{
    if( rf.check("name") ) {
	   moduleName = rf.find("name").asString();
    } else {
	   std::cerr << "MPCModule::configure failed: name parameter not found. Closing module." << std::endl;
	   return false;
    }

    if( rf.check("period") && rf.find("period").isInt()) {
       period = rf.find("period").asInt();
    } else {
       std::cerr << "MPCModule::configure failed: period parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("predictionTimeStep") && rf.find("predictionTimeStep").isInt()) {
       _predictionTimeStep = (float) rf.find("predictionTimeStep").asInt();
    } else {
       std::cerr << "MPCModule::configure failed: predictionTimeStep parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("controlTimeStep") && rf.find("controlTimeStep").isInt()) {
       _controlTimeStep = (float) rf.find("controlTimeStep").asInt();
    } else {
       std::cerr << "MPCModule::configure failed: controlTimeStep parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("com_pos") && rf.find("com_pos").isList() )
    {
        Bottle *com_ = rf.find("com_pos").asList();
        for (int i=0; i<3; i++)
            com(i) = (float) com_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: com_pos parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("com_vel") && rf.find("com_vel").isList() )
    {
        Bottle *comVel_ = rf.find("com_vel").asList(); 
        for (int i=0; i<3; i++)
            comVel(i) = (float) comVel_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: com_vel parameter not found. Closing module." << std::endl;
       return false;
    }
    
    if( rf.check("zmp_pos") && rf.find("zmp_pos").isList() )
    {
        Bottle *zmp_ = rf.find("zmp_pos").asList(); 
        for (int i=0; i<3; i++)
            zmp(i) = (float) zmp_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: zmp_pos parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("left_foot_pos") && rf.find("left_foot_pos").isList() )
    {
        Bottle *lFoot_ = rf.find("left_foot_pos").asList();
        for (int i=0; i<3; i++)
            lFoot(i) = (float) lFoot_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: left_foot_pos parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("left_foot_vel") && rf.find("left_foot_vel").isList() )
    {
        Bottle *lFootVel_ = rf.find("left_foot_vel").asList(); 
        for (int i=0; i<3; i++)
            lFootVel(i) = (float) lFootVel_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: left_foot_vel parameter not found. Closing module." << std::endl;
       return false;
    }
    
    if( rf.check("right_foot_pos") && rf.find("right_foot_pos").isList() )
    {
        Bottle *rFoot_ = rf.find("right_foot_pos").asList(); 
        for (int i=0; i<3; i++)
            rFoot(i) = (float) rFoot_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: right_foot_pos parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("right_foot_vel") && rf.find("right_foot_vel").isList() )
    {
        Bottle *rFootVel_ = rf.find("right_foot_vel").asList(); 
        for (int i=0; i<3; i++)
            rFootVel(i) = (float) rFootVel_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: right_foot_vel parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("footTofoot") && rf.find("footTofoot").isList() )
    {
        Bottle *footTofoot_ = rf.find("footTofoot").asList(); 
        for (int i=0; i<3; i++)
            footTofoot(i) = (float) footTofoot_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: footTofoot parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("com_vel_ref") && rf.find("com_vel_ref").isList() )
    {
        Bottle *comVelRef_ = rf.find("com_vel_ref").asList(); 
        for (int i=0; i<3; i++)
            comVelRef(i) = (float) comVelRef_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: com_vel_ref parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("omega") && rf.find("omega").isDouble()) {
       omega =(float) rf.find("omega").asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: omega parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("footSize") && rf.find("footSize").isList() )
    {
        Bottle *footSize_ = rf.find("footSize").asList(); 
        for (int i=0; i<2; i++)
            footSize(i) = (float) footSize_->get(i).asDouble();
    } else {
       std::cerr << "MPCModule::configure failed: footSize parameter not found. Closing module." << std::endl;
       return false;
    }
    
    if( rf.check("N") && rf.find("N").isInt()) {
       N = rf.find("N").asInt();
    } else {
       std::cerr << "MPCModule::configure failed: N parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("n") && rf.find("n").isInt()) {
       n = rf.find("n").asInt();
    } else {
       std::cerr << "MPCModule::configure failed: n parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("m") && rf.find("m").isInt()) {
       m = rf.find("m").asInt();
    } else {
       std::cerr << "MPCModule::configure failed: m parameter not found. Closing module." << std::endl;
       return false;
    }

    if( rf.check("q") && rf.find("q").isInt()) {
       q = rf.find("q").asInt();
    } else {
       std::cerr << "MPCModule::configure failed: q parameter not found. Closing module." << std::endl;
       return false;
    }
    

    //--------------------------RPC PORT--------------------------------------------
    attach(rpcPort);
    std::string rpcPortName= "/";
    rpcPortName+= getName();
    rpcPortName += "/rpc:i";
    if (!rpcPort.open(rpcPortName.c_str())) {
	   std::cerr << getName() << ": Unable to open port " << rpcPortName << std::endl;
	   return false;
    }
    

    mpcThread = new MPCThread(moduleName, period, _predictionTimeStep, _controlTimeStep, 
                  com, comVel, zmp,  
                  lFoot, lFootVel, 
                  rFoot, rFootVel,
                  footTofoot, comVelRef,
                  omega, footSize,
                  N, n, m, q);
    if(!mpcThread->start())
    {
	   yError() << getName()
	                  << ": Error while initializing LIP thread."
	                  << "Closing module";
	   return false;
    }

    yInfo("MPCThread started. (Running at %d ms)",period);


    return true;
} 

 
bool MPCModule::interruptModule()                       
{
    if(mpcThread)
    	mpcThread->suspend();
        rpcPort.interrupt();
    return true;
}


bool MPCModule::close()                                 
{
    // Get for the last time time stats
    mpcThread->getEstPeriod(avgTime, stdDev);
    mpcThread->getEstUsed(avgTimeUsed, stdDevUsed);     // real duration of run()
    
    //closing ports
    std::cout << getName() << ": closing RPC port interface" << std::endl;
    rpcPort.close();


    printf("[PERFORMANCE INFORMATION]:\n");
    printf("Expected period %d ms.\nReal period: %3.1f+/-%3.1f ms.\n", period, avgTime, stdDev);
    printf("Real duration of 'run' method: %3.1f+/-%3.1f ms.\n", avgTimeUsed, stdDevUsed);
    if(avgTimeUsed<0.5*period)
	   printf("Next time you could set a lower period to improve the retargeting performance.\n");
    else if(avgTime>1.3*period)
	   printf("The period you set was impossible to attain. Next time you could set a higher period.\n");

    //stop threads
    if(mpcThread)
    {
        yInfo() << getName() << ": closing MPCThread";
        mpcThread->closePort();
        mpcThread->stop();
        delete mpcThread;
        mpcThread = 0;
    }

    return true;
}


bool MPCModule::updateModule()
{
    if (mpcThread==0)
    {
    	yError("retargetingThread pointers are zero\n");
    	return false;
    }

    mpcThread->getEstPeriod(avgTime, stdDev);
    mpcThread->getEstUsed(avgTimeUsed, stdDevUsed);     // real duration of run()

    if(avgTime > 1.3 * period)
    {
    	yWarning("[WARNING] MPC solver loop is too slow. Real period: %3.3f+/-%3.3f. Expected period %d.\n", avgTime, stdDev, period);
    	yInfo("Duration of 'run' method: %3.3f+/-%3.3f.\n", avgTimeUsed, stdDevUsed);
    }
    return true;
}

 
bool MPCModule::quit()
{
    return this->close();
}




